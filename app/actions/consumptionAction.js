import {connect} from 'react-redux';
import * as ActionTypes from './actionTypes';
import consumption from '../scenes/consumption';
import moment from 'moment';

// All state pass to reducer
const mapStateToProps = state => ({
  data: state.consumptionReducer.data,
  totalAmount: state.consumptionReducer.totalAmount,
  totalEnergy: state.consumptionReducer.totalEnergy,
  activeTab: state.consumptionReducer.activeTab,
  currentDate: state.consumptionReducer.currentDate,
  isLoading: state.consumptionReducer.isLoading,
  error: state.consumptionReducer.error,
});

// Dispatch to action to particular function
const mapDispatchToProps = dispatch => ({
  tab: currentTab => {
    dispatch(selectTab(currentTab));
  },
  getData: (currentDate, args) => {
    dispatch(getResponse(currentDate, args));
  },
});


//Get the result from Api by passing request params and dispatch
export const getResponse = (date, args) => {
  if (args === 'prev') {
    var currentDate = moment(date).subtract(1, 'M');
  } else if (args === 'next') {
    var currentDate = moment(date).add(1, 'M');
  } else {
    var currentDate = date;
  }

  return dispatch => {
    dispatch(getData());

    const monthStartDate = moment(currentDate)
      .startOf('month')
      .utc();
    const monthEndDate = moment(currentDate)
      .endOf('month')
      .utc();

    monthStartDate.set({hour: 4, minute: 30, second: 0, millisecond: 0});
    monthStartDate.toISOString();
    monthStartDate.format();

    monthEndDate.set({hour: 4, minute: 30, second: 0, millisecond: 0});
    monthEndDate.toISOString();
    monthEndDate.format();

    const startDate = moment(monthStartDate)
      .add(2, 'd')
      .utc();
    const endDate = moment(monthEndDate)
      .add(1, 'd')
      .utc();

    return fetch('https://jsonrpc.getbarry.dk/json-rpc', {
      method: 'POST',
      body: JSON.stringify({
        jsonrpc: '2.0',
        id: '1',
        method:
          'co.getbarry.megatron.controller.ConsumptionFreemiumController.getDailyConsumptionWithPrice',
        params: [startDate, endDate, 'CET'],
      }),
    })
      .then(response => response.json())
      .then(jsonResponse => {
        const totalEnergy = jsonResponse.result
          .reduce(
            (accumulator, currentValue) => accumulator + currentValue.value,
            0,
          )
          .toFixed(2);
        const totalAmount = jsonResponse.result
          .reduce(
            (accumulator, currentValue) =>
              accumulator + currentValue.priceIncludingVat,
            0,
          )
          .toFixed(2);

        dispatch(
          getSucessData(jsonResponse, totalEnergy, totalAmount, currentDate),
        );
      })
      .catch(error => {
        console.error(error);
        dispatch(getDataFail(error));
      });
  };
};

export const selectTab = currentTab => {
  return dispatch => {
    var activeTab = currentTab;
    return dispatch(selectedTab(activeTab));
  };
};
// Pass active tab parameter to reducer
export const selectedTab = activeTab => ({
  type: ActionTypes.ACTIVE_TAB,
  activeTab: activeTab,
});

// Pass API response data to Reducer by action type
export const getSucessData = (data, totalEnergy, totalAmount, currentDate) => ({
  type: ActionTypes.GET_DATA_SUCCESS,
  data: data,
  currentDate: currentDate,
  totalEnergy: totalEnergy,
  totalAmount: totalAmount,
});

// Pass error message to reducer by action type
export const getDataFail = error => ({
  type: ActionTypes.GET_DATA_FAIL,
  error: error,
});
// Pass response data to reducer by action type
export const getData = () => ({
  type: ActionTypes.GET_DATA,
});

// Connect state and props to component
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(consumption);
