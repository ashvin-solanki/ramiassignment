/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  ActivityIndicator,
  View,
  FlatList,
  TouchableOpacity,
  StatusBar,
  Image,
  SafeAreaView,
  Alert,
  StyleSheet
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';

const appName = 'GetBarry';
const next = 'next';
const prev = 'prev';
const unit = ' kWh';
const currency = ' kr.';


class Consumption extends Component {
  //load initial data of current month
  componentDidMount = () => this.props.getData(this.props.currentDate);
  // Display data into AlertBox
  AlertBox = (date, charge) => {
    if (this.props.activeTab === 1) {
      Alert.alert(appName, 'consumed ' + charge + unit + ' on ' + date);
    } else if (this.props.activeTab === 0) {
      Alert.alert(appName, 'consumed ' + charge + currency +' charge on ' + date);
    }
  };
  // Dispaly total of monthly unit in Kwh and currency
  total = () => {
    return (
      <View style={styles.sumView}>
        <Text
          style={[
            styles.sumTextStyle,
            {color: this.props.activeTab === 1 ? '#FF4B9B' : '#EF7B40'},
          ]}>
          {this.props.activeTab === 1
            ? this.props.totalEnergy
            : this.props.totalAmount}
          <Text style={styles.sumUnitTextStyle}>
            {this.props.activeTab === 1 ? unit : currency}
          </Text>
        </Text>
      </View>
    );
  };
  // Convert date formate to 'DD/MM YYYY'
   dateFormat = date =>
   moment(date)
     .utc()
     .format('DD/MM YYYY');
 // Convert Month name and year from given date
 monthName = date =>
   moment(date)
     .utc()
     .format('MMMM YYYY');
  // Call this function when no data available
  emptyList = () => {
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyText}>{this.props.error}</Text>
      </View>
    );
  };
  // This is top section view to display calendar with left/Right buttons
  topSection = () => {
    return (
      <View style={styles.topSection}>
        <View style={styles.titleView}>
          <Text style={styles.titleText}>Your consumption</Text>
        </View>

        <View style={styles.topMiddleView}>
        
            <TouchableOpacity
              style={styles.leftArrow}
              onPress={() => this.props.getData(this.props.currentDate, prev)}>
              <Image
                source={require('../assets/images/leftArrow.png')}
                style={styles.arrowIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          <View style={styles.currentMonthView}>
            <Text style={styles.monthText}>
              {this.monthName(this.props.currentDate)}
            </Text>
          </View>
            <TouchableOpacity
              style={styles.rightArrow}
              onPress={() => this.props.getData(this.props.currentDate, next)}>
              <Image
                source={require('../assets/images/rightArrow.png')}
                style={styles.arrowIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
        </View>

        <View style={styles.tabView}>
          <View style={styles.button}>
            <TouchableOpacity
              style={[
                styles.tabClick,
                {
                  backgroundColor:
                    this.props.activeTab === 1 ? '#FF4B9B' : '#32363D',
                },
              ]}
              onPress={() => this.props.tab(1)}>
              <Text style={styles.buttonText}>{unit}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.tabClick,
                {
                  backgroundColor:
                    this.props.activeTab === 0 ? '#EF7B40' : '#32363D',
                },
              ]}
              onPress={() => this.props.tab(0)}>
              <Text style={styles.buttonText}>{currency}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  // this section display the data into flatlist
  bottomSection = () => {
    return (
      <View style={styles.bottomSection}>
          <FlatList
           refreshing={this.props.isLoading}
            ListHeaderComponent={this.total}
            data={this.props.data.result}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.activeTab === 1
                    ? this.AlertBox(
                        this.dateFormat(item.date),
                        item.value.toFixed(2),
                      )
                    : this.AlertBox(
                        this.dateFormat(item.date),
                        item.priceIncludingVat.toFixed(2),
                      )
                }
                style={styles.singleBox}>
                <View style={styles.date}>
                  <Text style={styles.dateText}>
                    {this.dateFormat(item.date)}
                  </Text>
                </View>
                <View style={styles.unitBox}>
                  <Text style={styles.valueStyle}>
                    {this.props.activeTab === 1
                      ? item.value.toFixed(2)
                      : item.priceIncludingVat.toFixed(2)}
                    <Text style={styles.unitTextStyle}>
                      {this.props.activeTab === 1 ? unit : currency}
                    </Text>
                  </Text>
                  <Image
                      source={require('../assets/images/unitArrow.png')}
                      style={styles.unitArrow}
                      resizeMode="contain"
                    />
                </View>
              </TouchableOpacity>
            )}
            ListEmptyComponent={this.emptyList}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
    );
  };

  loader = () =>{
    return(
      <View style={styles.loadingView}>
          <ActivityIndicator animating={this.props.isLoading} size="large" />
        </View>
    )
  }

  // Render Design of main screen
  render = () => {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="#2C2F36" barStyle="light-content" />
        {this.loader()}
        {this.topSection()}
        {this.bottomSection()}
      </SafeAreaView>
    );
  };
}
// Validate Prop value  with proptype
Consumption.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  totalEnergy: PropTypes.string.isRequired,
  error: PropTypes.string.isRequired,
  totalAmount: PropTypes.string.isRequired,
  activeTab: PropTypes.number.isRequired,
  currrentDate: PropTypes.instanceOf(Date),
};

const styles = StyleSheet.create({
  container: {
    flex: 10,
    justifyContent: 'center',
    backgroundColor: '#2C2F36',
  },
  loadingView: {
    position: 'absolute',
    width: '100%',
  },
  topSection: {
    flex: 3,
    shadowColor: '#05232d',
    elevation: 2,
    borderColor: '#FFFFFF',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10,
  },
  titleView: {
    flex: 1,
    borderColor: '#000000',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },

  sumView: {
    paddingTop: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#FFFFFF',
  },
  sumTextStyle: {
    fontSize: 32,
    fontWeight: 'bold',
    lineHeight: 38,
    alignItems: 'center',
    display: 'flex',
  },
  button: {
    flexDirection: 'row',
    backgroundColor: '#32363D',
    borderRadius: 18,
  },
  tabClick: {
    width: 160,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  buttonText: {
    fontSize: 12,
    lineHeight: 16,
    color: '#ffffff',
    fontWeight: '800',
  },
  unitTextStyle: {
    fontSize: 10,
    opacity: 0.8,
    fontWeight: 'bold',
  },
  titleText: {
    fontWeight: '600',
    fontSize: 18,                          
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  bottomSection: {
    flex: 7,
    borderColor: '#FFFFFF',
  },
  sumUnitTextStyle: {
    fontSize: 16,
    color: '#ffffff',
    opacity: 0.6,
    fontWeight: 'bold',
  },
  emptyBox: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 300,
  },
  emptyText: {
    fontSize: 15,
    color: '#ffffff',
  },
  unitText: {
    fontSize: 16,
    lineHeight: 19,
    fontWeight: 'bold',
    opacity: 0.6,
    color: '#ffffff',
  },
  singleBox: {
    borderWidth: 0,
    borderBottomColor: '#373940',
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: 60,
    flex: 10,
  },
  unitBox: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:"row",
    borderColor: '#FFFFFF',
    borderWidth: 0,
    
  },
  date: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderColor: '#FFFFFF',
    borderWidth: 0,
    paddingLeft: 30,
  },
  dateText: {
    color: '#FFFFFF',
    fontSize: 14,
    lineHeight: 17,
    fontWeight: 'bold',
    alignItems: 'flex-start',
  },
  valueStyle: {
    color: '#FFFFFF',
    fontSize: 14,
    lineHeight: 17,
    fontWeight: 'bold',
    alignItems: 'flex-end',
  },
  unitArrow: {
    width: 6,
    height: 9,
    margin:10
  },
  topMiddleView: {
    flex: 1,
    borderColor: '#000000',
    borderWidth: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  arrowIcon: {
    width: 10,
    height: 16,
  },
  currentMonthView: {
    flex: 0.8,
  },
  leftArrow: {
    flex: 0.1,
    padding: 20,
  },
  tabView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#000000',
    borderWidth: 0,
  },
  monthText: {
    fontWeight: '600',
    fontSize: 18,
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#FFFFFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  rightArrow: {
    flex: 0.1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 20,
  },
 
  
})

export default Consumption;
