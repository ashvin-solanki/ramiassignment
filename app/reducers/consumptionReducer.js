import * as Actions from '../actions/actionTypes';

// define initial state with initial value
const initialState = {
  isLoading: true,
  error: 'No record found',
  data: {},
  currentDate: new Date(),
  activeTab: 1,
  totalEnergy: '',
  totalAmount: '',
};
// Reducer update all state value
const consumptionReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.ACTIVE_TAB:
      return Object.assign({}, state, {
        isLoading: false,
        activeTab: action.activeTab,
      });
    case Actions.GET_DATA:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case Actions.GET_DATA_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        data: action.data,
        totalEnergy: action.totalEnergy,
        totalAmount: action.totalAmount,
        currentDate: action.currentDate,
      });
    case Actions.GET_DATA_FAIL:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error,
      });
    default:
      return state;
  }
};
export default consumptionReducer;
