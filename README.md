Project Name
-------------------------------
myassignment

Used React Native and other Installed packaged with version
-------------------------------
React native Version : "0.62.2",
React : "16.11.0"
moment : "^2.25.1",
redux-thunk: "2.3.0"
redux : "4.0.5",
react-redux : "7.2.0",
prop-types: "15.7.2"   

How to install and Run project 
-------------------------------
Please follow the below step.
1) go to your project and install yarn
2) command "react-native start" for start metro server
3) Run project in Android using command "react-native run-android"
4) Run Project in iOS using command "react-native run-ios"

How to work 
-------------------------------
1) Initially display the current month of data 
2) By Left and right, User can see next and previos month of data. 
3) User can switch power and rate by clicking on individual button.
4) Use can see the consumed data by clicking on single row in AlertBox.
5) "No Record found" message will show if no data comes from Api.




